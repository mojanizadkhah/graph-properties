function [out,LAG] = myCorr(out1,out2,out3,out4,out5,t)
    x = t;
    xq = t(1):0.1:t(13);

    method = 'spline';
    out1 = interp1(x,out1,xq,method);
    out2 = interp1(x,out2,xq,method);
    out3 = interp1(x,out3,xq,method);
    out4 = interp1(x,out4,xq,method);
    out5 = interp1(x,out5,xq,method);
    
    [c,lags] = xcorr(out1,out2,'coeff');
    [out(1),I] = max(c);
    LAG(1) = lags(I);
    
    [c,lags] = xcorr(out1,out3,'coeff');
    [out(2),I] = max(c);
    LAG(3) = lags(I);
    
    [c,lags] = xcorr(out1,out4,'coeff');
    [out(3),I] = max(c);
    LAG(3) = lags(I);
    
    [c,lags] = xcorr(out1,out5,'coeff');
    [out(4),I] = max(c);
    LAG(4) = lags(I);
    
    [c,lags] = xcorr(out2,out3,'coeff');
    [out(5),I] = max(c);
    LAG(5) = lags(I);
    
    [c,lags] = xcorr(out2,out4,'coeff');
    [out(6),I] = max(c);
    LAG(6) = lags(I);
    
    [c,lags] = xcorr(out2,out5,'coeff');
    [out(7),I] = max(c);
    LAG(7) = lags(I);
    
    [c,lags] = xcorr(out3,out4,'coeff');
    [out(8),I] = max(c);
    LAG(8) = lags(I);
    
    [c,lags] = xcorr(out3,out5,'coeff');
    [out(9),I] = max(c);
    LAG(9) = lags(I);
    
    [c,lags] = xcorr(out4,out5,'coeff');
    [out(10),I] = max(c);
    LAG(10) = lags(I);
    
    LAG = LAG/10;
end