function out = meanDistanse(mat)
    [lx, ly, lt] = size(mat);
    out = zeros(lt,1);

    for tt = 1:lt
        tmat(:,:) = mat(:,:,tt);
        [S,T,W] = myGraph(tmat);
        G = digraph(S,T,W);
        d = distances(G);
        a = find(d==Inf);
        d(a)=NaN;
        out(tt) = mean(mean(d,'omitnan'));
    end
end