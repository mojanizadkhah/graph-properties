function [node,edge] = Gbetweenness(mat)
    node = zeros(1,length(mat(1,1,:)));
    edge = zeros(1,length(mat(1,1,:)));
    for tt = 1:length(mat(1,1,:))        
        y(:,:) = mat(:,:,tt);
        s =sparse(y);
        [bc,E] = betweenness_centrality(s);
        
        node(tt) = mean(bc);
        edge(tt) = mean(full(E),'all');
        
%         ucc1 = centrality(G,'betweenness')
    end
end