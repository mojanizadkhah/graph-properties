%%
function out = myMaxFlow(mat)
    [lx, ly, lt] = size(mat);
    out = zeros(lt,1);

    for tt = 1:lt
        tt
        tmat(:,:) = mat(:,:,tt);
        [S,T,W] = myGraph(tmat);
        G = digraph(S,T,W);
        count = length(S);
        mf = zeros(count);
        for i = 1:count
            for j = 1:count
                if(S(i)~=T(j))
                    mf(i,j) = maxflow(G,S(i),T(j));
                end
            end
        end
        out(tt) = sum(mf,'all');            
    end
end