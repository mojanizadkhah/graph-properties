function out = myPlotAS(out1,out2,name,t)
    x = t;
    xq = t(1):0.1:t(13);

    method = 'spline';
    vq1 = interp1(x,out1,xq,method);
    vq2 = interp1(x,out2,xq,method);
    
    mdl1 = fitlm(t,out1);
    y1 = x*mdl1.Coefficients.Estimate(2)+mdl1.Coefficients.Estimate(1);
    
    mdl2 = fitlm(t,out2);
    y2 = x*mdl2.Coefficients.Estimate(2)+mdl2.Coefficients.Estimate(1);


    figure();
    hold all
    plot(xq,vq1,'r');
    plot(xq,vq2,'g');
    
    plot(x,y1,'r');
    plot(x,y2,'g');

    scatter(x,out1,'filled','r');
    scatter(x,out2,'filled','g');

    xlabel("time(month)");
    ylabel(name);

    legend("avoidance","submission");
    out=1;
    title(["interpolation(spline) ", name]);
    sname = [name,'ASinterpSpline.png'];
    saveas(gcf,sname);
end