function [S,T,W] = myGraph(tmat)

    [lx, ly] = size(tmat);
    lenn = sum((tmat>0),'all');
    S = zeros(lenn,1);
    T = zeros(lenn,1);
    W = zeros(lenn,1);
    count = 0;
    for ii = 1:lx
        for jj = 1:ly
            if(tmat(ii,jj))
                count = count+1;
                S(count)=ii;
                T(count)=jj;
                W(count)=tmat(ii,jj);
            end
        end
    end
end