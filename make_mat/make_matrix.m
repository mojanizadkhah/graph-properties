%%
% clc;
% clear;
a="";
name = table2cell(names);
for i = 1:2
    for j = 1:29619
        a( (i-1)*29619+j) = char(name{j,i});

    end
end
a = unique(a);
an = 1:1:76;

save a;
save an;
%% start
load a;
load an;
%% importing the data
% a = find(a=='BaiLian')
% HRM = xlsread("Hainan_Rhesus_Macaque_Social_interaction.xlsx");
HH = HainanRhesusMacaqueSocialinteraction;
H = table2cell(HH);
%%
taggression = 1:1:1663;
tapproach = 1664:1:18314;
tavoidance = 18315:1:19113;
tgrooming = 19114:1:29121;
tsubmission = 29122:1:29619;
clc
for i = 1:29619
    if(char(H{i,4})=="submission")
        i
    end
end
%%

n_node = 76;
count_per = 13;
MATaggression = zeros(n_node,n_node,count_per);
for i = taggression
    c_per = char(H{i,1});
    n_per = str2double(c_per(7:8)); 
    actor = char(H{i,5});
    n_actor = find(a==actor);
    resiever = char(H{i,6});
    n_res = find(a==resiever);
    MATaggression(n_actor,n_res,n_per) =MATaggression(n_actor,n_res,n_per)+1;    
end    
%%
save MATaggression;   
save MATapproach;
save MATavoidance;
save MATgrooming;
save MATsubmission;
%%
t_year = [13,14,14,14,15,15,15,16,16,16,17,17,17];
t_mon = [10,4,7,12,4,7,12,5,8,11,5,7,11];
t = 12*t_year+t_mon;
t = t - t(1);
% clearvars -except t;