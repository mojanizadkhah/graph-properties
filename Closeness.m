function [outin,outout] = Closeness(mat)
    [lx, ly, lt] = size(mat);
    
    outin = zeros(lt,1);
    outout = zeros(lt,1);
    
    for tt = 1:lt
        tmat(:,:) = mat(:,:,tt);
        [S,T,W] = myGraph(tmat);
        G = digraph(S,T,W);
        outin(tt) = mean(centrality(G,'incloseness'));
        outout(tt) = mean(centrality(G,'outcloseness'));
    end
    
end