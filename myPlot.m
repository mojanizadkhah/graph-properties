function out = myPlot(out1,out2,out3,out4,out5,name,t)
    x = t;
    xq = t(1):0.1:t(13);

    method = 'spline';
    vq1 = interp1(x,out1,xq,method);
    vq2 = interp1(x,out2,xq,method);
    vq3 = interp1(x,out3,xq,method);
    vq4 = interp1(x,out4,xq,method);
    vq5 = interp1(x,out5,xq,method);

    figure();
    hold all
    plot(xq,vq1,'r');
    plot(xq,vq2,'c');
    plot(xq,vq3,'g');
    plot(xq,vq4,'m');
    plot(xq,vq5,'b');

    scatter(x,out1,'filled','r');
    scatter(x,out2,'filled','c');
    scatter(x,out3,'filled','g');
    scatter(x,out4,'filled','m');
    scatter(x,out5,'filled','b');
    
    xlabel("time(month)");
    ylabel(name);

    legend("aggression","approach","avoidance","grooming","submission");

    title(["interpolation(spline) ", name]);
    sname = [name,'interpSpline.png'];
    saveas(gcf,sname);
    out = 1;
end