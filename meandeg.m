function out = meandeg(mat)
    t = length(mat(1,1,:));
    out = zeros(t,1);
    for i = 1:t
        out(i,1) = mean(mat(:,:,i),'all');
    end
end