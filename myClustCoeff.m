function out = myClustCoeff(mat)
    t = length(mat(1,1,:));
    out = zeros(t,1);    
    for i = 1:t
        [~,out(i), ~] = clustCoeff(mat(:,:,i));
    end
end