function [outR,outE] = myPlotReg(out1,out2,out3,out4,out5,name,t)
    x = t;
    
    mdl1 = fitlm(t,out1);
    y1 = x*mdl1.Coefficients.Estimate(2)+mdl1.Coefficients.Estimate(1);
    
    mdl2 = fitlm(t,out2);
    y2 = x*mdl2.Coefficients.Estimate(2)+mdl2.Coefficients.Estimate(1);
    
    mdl3 = fitlm(t,out3);
    y3 = x*mdl3.Coefficients.Estimate(2)+mdl3.Coefficients.Estimate(1);
    
    mdl4 = fitlm(t,out4);
    y4 = x*mdl4.Coefficients.Estimate(2)+mdl4.Coefficients.Estimate(1);
   
    mdl5 = fitlm(t,out5);
    y5 = x*mdl5.Coefficients.Estimate(2)+mdl5.Coefficients.Estimate(1);

    figure();
    hold all
    plot(x,y1,'r');
    plot(x,y2,'c');
    plot(x,y3,'g');
    plot(x,y4,'m');
    plot(x,y5,'b');

    scatter(x,out1,'filled','r');
    scatter(x,out2,'filled','c');
    scatter(x,out3,'filled','g');
    scatter(x,out4,'filled','m');
    scatter(x,out5,'filled','b');
    
    xlabel("time(month)");
    ylabel(name);

    legend("aggression","approach","avoidance","grooming","submission");
    outR= [mdl1.Rsquared.Ordinary,mdl2.Rsquared.Ordinary,mdl3.Rsquared.Ordinary,mdl4.Rsquared.Ordinary,mdl5.Rsquared.Ordinary];
    outE= [mdl1.Coefficients.Estimate(2),mdl2.Coefficients.Estimate(2),mdl3.Coefficients.Estimate(2),mdl4.Coefficients.Estimate(2),mdl5.Coefficients.Estimate(2)];
    
    title(["linear regression ", name]);
    sname = [name,'linearReg.png'];
    saveas(gcf,sname);
end